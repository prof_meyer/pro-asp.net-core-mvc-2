# Pro ASP.NET Core MVC 2

This repository accompanies [Pro ASP.NET Core MVC 2 by Adam Freeman (Apress, 2017)](https://www.apress.com/us/book/9781484231494).

Chapter solution files can be found in the author's GitHub repository here: [https://github.com/Apress/pro-asp.net-core-mvc-2](https://github.com/Apress/pro-asp.net-core-mvc-2)

This repository is intended to be used for educational purposes only, and is intended to supplement the use of Pro ASP.NET Core MVC 2 as a course textbook.

## Organization

The files contained within this repository are the chapter starting files, along with the step-by-step listings. 

The **master** branch will contain the starting files for each chapter.

Modifications to individual files are organized by their textbook **listings**.  Each chapter listing will be its own branch, beginning from the master branch.

**Example Structure:**

- **[master](https://gitlab.com/prof_meyer/pro-asp.net-core-mvc-2/tree/master)** [(changes)](https://gitlab.com/prof_meyer/pro-asp.net-core-mvc-2/compare/master...listing_2-2)
    - **[listing_2-2](https://gitlab.com/prof_meyer/pro-asp.net-core-mvc-2/tree/listing_2-2)** [(changes)](https://gitlab.com/prof_meyer/pro-asp.net-core-mvc-2/compare/listing_2-2...listing_2-3)
        - **[listing_2-3](https://gitlab.com/prof_meyer/pro-asp.net-core-mvc-2/tree/listing_2-3)** [(changes)](https://gitlab.com/prof_meyer/pro-asp.net-core-mvc-2/compare/listing_2-3...listing_2-4)


This pattern should help organize the changes and allow users to retrieve working files at any step for a given chapter.


